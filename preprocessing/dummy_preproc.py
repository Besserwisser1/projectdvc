import pandas as pd

data = pd.read_csv('data/raw/train.csv', header=None)
data.columns = ['mark', 'title', 'message']
data.to_parquet('data/preprocessed/train.parquet')